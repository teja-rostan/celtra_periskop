from video_flows_set import get_video_flows
import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import vonmises
import pandas as pd


# =========================
# = load data and streams =
# =========================
streams, df, names, field_views, threshold = get_video_flows(uniform=True)
df['angle_neg'] = df['angle']
df['angle'] = df['angle'].apply(lambda x: x + 360 * (x < 0))


def main():
    global streams, df, names, field_views, threshold
    m = np.random.random() * 60
    print(m)

    names = np.array(names)
    np.random.shuffle(names)
    df = df.loc[df.time >= m]
    df.time = df.time.round(decimals=1)
    scores = np.zeros((len(names), 1))

    for i, name in enumerate(names):
        # ===============================
        # = split to train and test set =
        # ===============================
        test = df.loc[df.index == name]
        train = df.loc[df.index != name]
        pred_quality = np.zeros((test.time.shape[0], len(streams)))

        for j, t in enumerate(test.time):
            # =========================================
            # = get mean and kappa=1/sd^2 and predict =
            # =========================================
            t_train = train.loc[train.time == t]
            t_angle = t_train.angle.as_matrix()
            sin_a = np.sin(np.radians(t_angle)).sum()
            cos_a = np.cos(np.radians(t_angle)).sum()
            atan2_a = np.arctan2(sin_a, cos_a)
            R = np.sqrt(np.square(sin_a) + np.square(cos_a))/len(t_angle)
            kappa = 1 / (-2 * np.log(R))
            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        # df_quality = pd.DataFrame(pred_quality, index=test.time, columns=get_stream_names())
        scores[i] = get_score(pred_quality, test)
    print(np.mean(scores))
    print(np.mean(scores)/(df.time.shape[0] * 60 * 100))


def get_score(pred, test):
    score = 0
    for i, angle in enumerate(test.angle):
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        field_view = field_view * np.logical_and(0 <= field_view, field_view < 360).astype(int) + \
                                  (field_view + 360) * (field_view < 0).astype(int) + \
                                  (field_view - 360) * (field_view > 359).astype(int)
        for view in field_view:
            score += pred[i, int(view/6)]
    return score


def get_video_quality(kappa, loc):
    # ==============
    # = parameters =
    # ==============
    B = 10000
    high = 100
    low = 1
    x = np.linspace(-np.pi, np.pi, 60)
    y = vonmises.pdf(x, kappa, loc)

    # ==========================================================
    # = quality distribution processing to meet our boundaries =
    # ==========================================================
    y = np.minimum(y, np.percentile(y, 90))
    mins = np.min(y)
    maxs = np.max(y)
    rng = maxs - mins
    y = np.round(high - (((high - low) * (maxs - y)) / rng)).astype(np.int)
    unique, counts = np.unique(y, return_counts=True)
    counts = dict(zip(unique, counts))
    to_cut = (sum(y)*5-B)/5
    idx = 2
    while to_cut > 0:
        if idx in counts:
            to_cut -= counts[idx] * (idx-1)
        idx += 1
    y[y < idx] = 1
    if sum(y)*5 > B:
        print("WAAAAAAAAAAAAAAAAAAAA!!!!!!!!!:", y, sum(y*5))
        plt.plot(np.degrees(x), y)
        plt.show()
    return y


def get_all_capacity():
    global streams
    capacity = 0
    for s in streams:
        capacity += s.get_capacity()
    return capacity


def get_stream_names():
    s_names = []
    for s in streams:
        s_names.append(s.name)
    return s_names


def get_all_low_quality():
    global streams
    capacity = 0
    for s in streams:
        capacity += s.get_capacity() * (s.quality == 1)
    return capacity


if __name__ == '__main__':
    main()


