from video_flows_set import get_uniform_streams
import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import vonmises
import pandas as pd
import scipy.ndimage
from seqlearn import hmm


# =========================
# = load data and streams =
# =========================


middle = 0
l = 0
half = 4
shift = 150

streams, df, names = get_uniform_streams(shift)

df_test = pd.read_csv("testData.csv", header=None, delimiter=',')
df_test.columns = ['name', 'time', 'angle']
# names = df_test['name'].unique().tolist()
df_test.set_index(keys=['name'], drop=True, inplace=True)


def main():
    pd.options.mode.chained_assignment = None
    global streams, df, names, field_views, threshold, middle, l
    grouped = df_test.groupby(df_test.index)
    pred_quality = np.zeros((len(df_test), 60))
    idx = 0
    for name, group in grouped:

        all_predictions = []
        argsort_group = group.time.argsort()
        group.sort_values("time", inplace=True)

        df_before_test = group.loc[pd.notnull(group.angle)]
        df_after_test = group.loc[pd.isnull(group.angle)]
        m = int(df_after_test.time.as_matrix()[0])
        middle = m

        predictions = get_score_from_known(df_before_test)
        all_predictions.extend(predictions)

        names = np.array(names)
        np.random.shuffle(names)
        avg_four_score = []
        for l_now in [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]:
            l = l_now

            df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
            df_after_dz = df.loc[np.logical_and(m + l - 1 <= df.time, df.time < m + l)]
            df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]

            scores_dz = dummy_zero(df_after_dz)
            scores_hmm = hmm_learner(df_before, df_after)
            perfect_scores = np.mean(df_after.groupby(df_after.index).size().as_matrix() * 60 * 100)
            chose = np.argmax([np.mean(scores_hmm), np.mean(scores_dz)])
            print("hmm:", np.mean(scores_hmm) / perfect_scores, "dz:", np.mean(scores_dz) / perfect_scores, "chose:", chose)

            avg_four_score.append(chose)
        is_empty = False
        for i, l_now in enumerate([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]):
            l = l_now

            df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
            df_after_dz = df_after_test.loc[np.logical_and(m + l - 0.5 <= df_after_test.time, df_after_test.time < m + l)]

            df_after_test_train = df_after_test.loc[np.logical_and(m <= df_after_test.time, df_after_test.time < m + l)]
            df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]
            df_before_test_train = df_before_test.loc[np.logical_and(m - l <= df_before_test.time, df_before_test.time < m)]

            if df_before_test_train.empty:
                print(group)
                is_empty = True
                continue

            if avg_four_score[i] == 0:
            # if True:
                predictions = hmm_learner_last(df_before, df_after, df_before_test_train, df_after_test_train)
                all_predictions.extend(predictions[:-len(df_after_dz)])
            else:
                predictions = dummy_zero_last(df_after_dz)
                all_predictions.extend(predictions[:-len(df_after_dz)])

        if is_empty:
            print(all_predictions)
        argsort_group = np.argsort(argsort_group.as_matrix())
        all_predictions = np.asarray(all_predictions)
        all_predictions = all_predictions[argsort_group]

        for p in all_predictions:
            pred_quality[idx] = p
            idx += 1
        # pred_quality.append(all_predictions)

    my_data = pd.DataFrame(data=pred_quality)
    my_data.to_csv("my_data_hist.csv")


def get_score_from_known(df_before_test):
    pred_quality = np.zeros((df_before_test.time.shape[0], len(streams)))
    for j, a in enumerate(df_before_test.angle):
        atan2_a = np.radians(a)
        atan2_a -= 2 * np.pi * (atan2_a > np.pi)
        kappa = 4
        y = get_video_quality(kappa, atan2_a)
        pred_quality[j] = y
    return pred_quality


def dummy_zero(df_after):
    global shift
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        pred_quality = np.zeros((df_after.time.shape[0], len(streams)))
        test = df_after.loc[df_after.index == name]
        for j, t in enumerate(test.time):
            angle = 0
            y = get_video_quality(4, angle - 360 * (angle > 359))
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, test)
    return scores


def dummy_zero_last(df_after):
    global shift
    pred_quality = np.zeros((df_after.time.shape[0], len(streams)))
    for j, t in enumerate(df_after.time):
        angle = 0
        y = get_video_quality(4, angle - 360 * (angle > 359))
        pred_quality[j] = y
    return pred_quality


def one_hot(x):
    x_new = np.zeros((x.shape[0], 60))
    for i, idx in enumerate(x):
        x_new[i, idx] = 1
    return x_new


def add_missing(df, m_type):
    global middle, half, l
    all = l*half+1
    df_new = df.reset_index()
    sizes = df_new.groupby([df_new.name]).size()
    # print(sizes)
    outliers = sizes.loc[sizes != all]
    for index, outlier in outliers.iteritems():
        first = list(df_new.loc[df_new.name == index].index)[0]
        times = df_new.loc[df_new.name == index].time.as_matrix()
        angles = df_new.loc[df_new.name == index].angle.as_matrix()
        # print(len(angles))
        if m_type == "before":
            before = np.arange(middle-l, middle+0.1, step=1/half)
            for i, b in enumerate(before):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print(m_type, times, before)
        elif m_type == "after":
            after = np.arange(middle, middle+l+0.1, step=1/half)
            for i, b in enumerate(after):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print(m_type, times, after)
    sizes = df_new.groupby([df_new.name]).size()
    outliers = sizes.loc[sizes != all]
    if len(outliers) != 0:
        print(outliers, m_type)
    return df_new.set_index(keys=['name'], drop=True)


def half_sec_round(df, m_type):
    global half
    df.time = df.time * half
    df.time = df.time.round()
    df.time = df.time / half
    df = df.groupby([df.index, 'time']).mean()
    df = df.reset_index()
    df.set_index(keys=['name'], drop=True, inplace=True)
    return add_missing(df, m_type)


def degree2stream(degrees):
    global shift
    degrees -= shift
    degrees += 360 * (degrees < 0)
    degrees -= 360 * (degrees > 359)
    return np.floor(degrees / 6)


def stream2degree(stream):
    global shift
    degrees = stream*6 + 2.5 + shift

    degrees += 360 * (degrees < 0)
    degrees -= 360 * (degrees > 359)
    return np.round(degrees)


def hmm_learner_last(df_before, df_after, df_before_test, y_test_df_time):
    global half
    y_test_df = half_sec_round(y_test_df_time, "after")
    y_train_df = half_sec_round(df_after, "after")
    X_test_df = half_sec_round(df_before_test, "before")
    X_train_df = half_sec_round(df_before, "before")
    lengths_train = y_train_df.groupby(level=0).size().as_matrix()
    lengths_test = X_test_df.groupby(level=0).size().as_matrix()

    X_train = one_hot(degree2stream(X_train_df.angle).astype(np.int))
    X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))
    y_train = degree2stream(y_train_df.angle).astype(np.int).as_matrix()

    clf = hmm.MultinomialHMM()

    clf.fit(X_train, y_train, lengths_train)
    # print(X_test.shape, lengths_test)
    y_pred = clf.predict(X_test, lengths_test)

    y_test_df["pred"] = y_pred[-1]
    pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
    for j, t in enumerate(y_test_df_time.time):
        approx_t = np.round(t * half) / half
        the_row = y_test_df.loc[y_test_df.time == approx_t]
        atan2_a = np.radians(stream2degree(the_row['pred'].as_matrix()[0]))
        atan2_a -= 2 * np.pi * (atan2_a > np.pi)
        kappa = 4

        y = get_video_quality(kappa, atan2_a)
        pred_quality[j] = y
    return pred_quality


def hmm_learner(df_before, df_after):
    global half, names
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        y_train_df = half_sec_round(df_after.loc[df_after.index != name], "after")
        y_train_df = y_train_df[y_train_df.time.isin(y_test_df.time)]
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")
        X_train_df = half_sec_round(df_before.loc[df_before.index != name], "before")
        X_train_df = X_train_df[X_train_df.time.isin(X_test_df.time)]
        lengths_train = y_train_df.groupby(level=0).size().as_matrix()
        lengths_test = y_test_df.groupby(level=0).size().as_matrix()

        X_train = one_hot(degree2stream(X_train_df.angle).astype(np.int))
        X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))
        y_train = degree2stream(y_train_df.angle).astype(np.int).as_matrix()

        clf = hmm.MultinomialHMM()
        clf.fit(X_train, y_train, lengths_train)
        y_pred = clf.predict(X_test, lengths_test)
        y_test_df["pred"] = y_pred
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(stream2degree(the_row['pred'].as_matrix()[0]))
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores


def get_score(pred, test):
    score = 0
    for i, angle in enumerate(test.angle):
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        field_view += 360 * (field_view < 0)
        field_view -= 360 * (field_view > 359)
        for view in field_view:
            # view = degree2stream(view)
            view = int(view/6)
            # print(view)
            score += pred[i, view]
    return score


def get_video_quality(kappa, loc):
    # ==============
    # = parameters =
    # ==============
    B = 10000
    high = 100
    low = 1
    x = np.linspace(-np.pi, np.pi, 60)
    y = vonmises.pdf(x, kappa, loc)
    y = np.minimum(y, np.percentile(y, 85))
    # high = np.maximum(np.minimum(np.round(np.max(y)*160), high), 32)

    # ==========================================================
    # = quality distribution processing to meet our boundaries =
    # ==========================================================
    # y = np.minimum(y, np.percentile(y, 98))
    # y = np.minimum(y, np.max(y) * 0.95)
    mins = np.min(y)
    maxs = np.max(y)
    rng = maxs - mins
    y = np.ceil(high - (((high - low) * (maxs - y)) / rng)).astype(np.int)
    # unique, counts = np.unique(y, return_counts=True)
    # counts = dict(zip(unique, counts))
    # to_cut = (sum(y) * 6 - B) / 6
    to_cut = (sum(y*6) - B)
    y_sort = np.argsort(y)
    idx=0
    while to_cut > 0:
        to_cut -= y[y_sort[idx]]*6 - low*6
        y[y_sort[idx]] = low
        idx += 1
    # idx = low+1
    # while to_cut > 0:
    #     if idx in counts:
    #         to_cut -= counts[idx] * (idx - low)
    #     idx += 1
    # y[y < idx] = low
    if sum(y) * 6 > B:
        print("ERROR!, sum of quality above upper bound!:", y, sum(y * 6), idx)
        plt.plot(np.degrees(x), y)
        plt.show()
    if B - sum(y)*6 > 280:
        print("WARNING!, sum of quality is low!:", B - sum(y*6))
    return np.roll(y, -(int(len(y)/2)))


if __name__ == '__main__':
    main()


