"""
 The program needs a "testData.csv" and "learnData.csv" and saves results at "my_data_qualities.csv".
 The results are the predicted angles for every time-point at "testData.csv".

 @author: Teja Roštan
"""
from video_flows_set import get_uniform_streams_and_train_data
import numpy as np
import pandas as pd
from seqlearn import hmm


# ====================#
# = Global variables =#
# ====================#
middle = 0
l = 0
half = 4
shift = 150

# =============================================================#
# = load data and streams (for error checking in HMM learner) =#
# =============================================================#
streams, df, names = get_uniform_streams_and_train_data(shift)
df_test = pd.read_csv("testData.csv", header=None, delimiter=',')
df_test.columns = ['name', 'time', 'angle']
df_test.set_index(keys=['name'], drop=True, inplace=True)


def main():
    pd.options.mode.chained_assignment = None
    global streams, df, names, middle, l

    # group tests to separate users
    grouped = df_test.groupby(df_test.index)
    pred_quality = np.zeros((len(df_test), 2))
    idx = 0
    for name, group in grouped:
        nanan = []
        all_predictions = []

        # some time-points of users were not in order
        argsort_group = group.time.argsort()
        group.sort_values("time", inplace=True)

        # split to known and unknown angles (m is the threshold time)
        df_before_test = group.loc[pd.notnull(group.angle)]
        df_after_test = group.loc[pd.isnull(group.angle)]
        m = int(df_after_test.time.as_matrix()[0])
        middle = m

        # get and store "predictions" of known angles
        predictions = get_score_from_known(df_before_test)
        all_predictions.extend(predictions)
        nanan.extend(np.zeros(predictions.shape))

        is_empty = False
        for l_now in [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]:
            l = l_now

            # get the slices of time-points of train and test data
            df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
            df_after_dz = df_after_test.loc[np.logical_and(m + l - 0.5 <= df_after_test.time, df_after_test.time < m + l)]
            df_after_test_train = df_after_test.loc[np.logical_and(m <= df_after_test.time, df_after_test.time < m + l)]
            df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]
            df_before_test_train = df_before_test.loc[np.logical_and(m - l <= df_before_test.time, df_before_test.time < m)]

            if df_before_test_train.empty:
                print("WARNING! df_before_test_train is empty!", group)
                is_empty = True
                continue

            # get the predictions of the time-point slice with hmm_learner
            predictions = hmm_learner_test(df_before, df_after, df_before_test_train, df_after_test_train)
            all_predictions.extend(predictions[-len(df_after_dz):])
            nanan.extend(np.ones(predictions[-len(df_after_dz):].shape))

        if is_empty:
            print("WARNING! df_before_test_train is empty!", all_predictions)

        # unsort the sorted time-points (if they were not ordered) and store predictions
        argsort_group = np.argsort(argsort_group.as_matrix())
        all_predictions = np.asarray(all_predictions)
        all_predictions = all_predictions[argsort_group]

        for n, p in zip(nanan, all_predictions):
            pred_quality[idx, 0] = p
            pred_quality[idx, 1] = n
            idx += 1

    my_data = pd.DataFrame(data=pred_quality)
    my_data.to_csv("my_data_half.csv")


def get_score_from_known(df_before_test):
    """
    Extracts angles as "predictions" (angles).
    :param df_before_test: A Data Frame with time-points (time and angle) to extract the angles.
    :return: "Predictions" of angles
    """
    pred_quality = np.zeros((df_before_test.time.shape[0], 1))
    for j, a in enumerate(df_before_test.angle):
        pred_quality[j] = a
    return pred_quality


def one_hot(x):
    """
    One hot encoding to (hardcoded) 60 possible streams (for HMM learning).
    :param x: A column array where rows are to be one hot encoded.
    :return: An Array with one hot encoded rows.
    """
    x_new = np.zeros((x.shape[0], 60))
    for i, idx in enumerate(x):
        x_new[i, idx] = 1
    return x_new


def add_missing(df, m_type):
    """
    Artificially adds a time-point if missing with mean value of angles from previous and next time-point.
    :param df: Data Frame with times and angles that we want to fill with missing time-points.
    :param m_type: As "before" or "after" of the middle point (middle time-point of ~8s of a sample).
    :return: New Data Frame where missing time-points are added.
    """
    global middle, half, l
    all_time_points = l*half+1
    df_new = df.reset_index()
    sizes = df_new.groupby([df_new.name]).size()
    outliers = sizes.loc[sizes != all_time_points]
    for index, outlier in outliers.iteritems():
        first = list(df_new.loc[df_new.name == index].index)[0]
        times = df_new.loc[df_new.name == index].time.as_matrix()
        angles = df_new.loc[df_new.name == index].angle.as_matrix()

        if m_type == "before":
            before = np.arange(middle-l, middle+0.1, step=1/half)
            for i, b in enumerate(before):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print("WARNING! at add_missing:", m_type, times, before)

        elif m_type == "after":
            after = np.arange(middle, middle+l+0.1, step=1/half)
            for i, b in enumerate(after):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print("WARNING! at add_missing:", m_type, times, after)

    sizes = df_new.groupby([df_new.name]).size()
    outliers = sizes.loc[sizes != all_time_points]
    if len(outliers) != 0:
        print("WARNING! at add_missing:", outliers, m_type)
    return df_new.set_index(keys=['name'], drop=True)


def half_sec_round(df, m_type):
    """
    Round time-points to 1/"half" seconds.
    :param df_to_round: Data Frame with times and angles where we want to round time-points to 1/"half.
    :param m_type: As "before" or "after" of the middle point that is forwarded to add_missing().
    :return: A Data-Frame with rounded time-points to 1/"half" seconds and with added of the missing.
    """
    global half
    df.time = df.time * half
    df.time = df.time.round()
    df.time = df.time / half
    df = df.groupby([df.index, 'time']).mean()
    df = df.reset_index()
    df.set_index(keys=['name'], drop=True, inplace=True)
    return add_missing(df, m_type)


def degree2stream(degrees):
    """
    Converts degrees to 60 uniformed streams for HMM learning.
    :param degrees: A value or an array of degrees.
    :return: A values or an array of streams.
    """
    global shift
    degrees -= shift
    degrees += 360 * (degrees < 0)
    degrees -= 360 * (degrees > 359)
    return np.floor(degrees / 6)


def stream2degree(stream):
    """
    Converts 60 uniformed streams to degrees (output of HMM learning).
    :param stream: A values or an array of streams.
    :return: A value or an array of degrees.
    """
    global shift
    degrees = stream*6 + 2 + shift
    degrees += 360 * (degrees < 0)
    degrees -= 360 * (degrees > 359)
    return np.floor(degrees)


def hmm_learner_test(df_before, df_after, df_before_test, y_test_df_time):
    """
    A supervised Hidden Markov model learning algorithm (seqlearn library) to make angle predictions 
    and predictions of 60 hardcoded stream qualities.
    :param df_before: A Data Frame of train data of time-points before the predictions.
    :param df_after: A Data frame of train data of time-points to predict.
    :param df_before_test: A Data Frame of test data of time-points before the predictions.
    :param y_test_df_time: A Data Frame of test data of time-points with Nan angles to predict.
    :return: The predicted 60 hardcoded stream qualities of test data.
    """
    global half

    # =====================================================#
    # = Prepare the X_train, y_train, X_test, y_test data =#
    # =====================================================#
    y_test_df = half_sec_round(y_test_df_time, "after")
    y_train_df = half_sec_round(df_after, "after")
    X_test_df = half_sec_round(df_before_test, "before")
    X_train_df = half_sec_round(df_before, "before")
    lengths_train = y_train_df.groupby(level=0).size().as_matrix()
    lengths_test = X_test_df.groupby(level=0).size().as_matrix()

    X_train = one_hot(degree2stream(X_train_df.angle).astype(np.int))
    X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))
    y_train = degree2stream(y_train_df.angle).astype(np.int).as_matrix()

    # ============================#
    # = Fit and predict with HMM =#
    # ============================#
    clf = hmm.MultinomialHMM()
    clf.fit(X_train, y_train, lengths_train)
    y_pred = clf.predict(X_test, lengths_test)
    y_test_df["pred"] = y_pred

    # ===========================#
    # = Store angle predictions =#
    # ===========================#
    pred_quality = np.zeros((y_test_df_time.time.shape[0]))
    for j, t in enumerate(y_test_df_time.time):
        approx_t = np.round(t * half) / half
        the_row = y_test_df.loc[y_test_df.time == approx_t]
        a = stream2degree(the_row['pred'].as_matrix()[0])
        pred_quality[j] = a
    return pred_quality


if __name__ == '__main__':
    main()


