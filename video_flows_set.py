"""
Prvi del vaše naloge je
določiti množico video tokov s_i = (α_i,β_i,q_i),
pri čemer vsak tok pokriva kot med α in β
(vkljucno) s kvaliteto q ∈ [1,100].
Video tokovi se lahko prekrivajo (izbere se tistega, ki ima najvišjo kvaliteto
Število video tokov, izbranih v prvem delu naloge, ne sme biti večje kot 100!
Imamo samo en videoposnetek in več uporabnikov.
"""
import numpy as np
import pandas as pd
# from matplotlib import pyplot as plt
import VideoFlow as vf

# shift = 150


def get_uniform_streams(shift):
    # global shift
    df, names = get_trajectories()
    streams = []
    for i in np.arange(start=0, stop=360, step=6):
        name = "stream" + str(int(i / 6))
        alpha = int(i) + shift
        beta = int(i) + 5 + shift
        alpha -= 360 * (alpha > 359)
        alpha += 360 * (alpha < 0)
        beta -= 360 * (beta > 359)
        beta += 360 * (beta < 0)
        quality = 1
        streams.append(vf.VideoFlow(name, alpha, beta, quality))
    for s in streams:
        s.get_info()
    video_stream_definition_csv(streams)
    return streams, df, names


def get_video_flows(uniform):
    global shift
    threshold = 150
    df, names = get_trajectories(threshold)

    # ==================================
    # = histogram of all viewed angles =
    # ==================================
    angles = df.angle
    # new_angles = get_field_view(angles)
    # new_angles = np.array(new_angles).flatten()
    # plt.hist(new_angles, bins=360, normed=True)
    # plt.title("Histogram with '360' degrees, the fieldview")
    # plt.axis([0, 359, 0, 0.01])

    # ==================================================
    # = viewed angle counts through time as data frame =
    # ==================================================
    df_plot = df.copy()
    df_plot.time = df_plot.time.round()
    new_angles = get_field_view(df_plot.angle, is_standard=False)
    df_plot["viewfield"] = new_angles
    field_views = concat_times(df_plot, threshold)
    streams = []
    if uniform:
        # =========================
        # = split video uniformly =
        # =========================
        for i in np.arange(start=0, stop=360, step=6):
            name = "stream" + str(int(i / 6))
            # alpha = int(i)
            alpha = int(i) + shift
            # beta = int(i) + 5
            beta = int(i) + 5 + shift
            alpha -= 360 * (alpha > 359)
            alpha += 360 * (alpha < 0)
            beta -= 360 * (beta > 359)
            beta += 360 * (beta < 0)
            quality = 1
            streams.append(vf.VideoFlow(name, alpha, beta, quality))
    else:
        # =======================================
        # = get split lines of angles for video =
        # =======================================
        lines = []
        Z = field_views.as_matrix().T
        Z = (Z - Z.min()) / (Z.max() - Z.min())
        y = np.arange(-(359 - threshold), threshold + 1)
        splits = np.round(np.linspace(0, len(field_views), 10))
        ranges = splits[1:] - splits[:-1]
        for i, r in enumerate(ranges):
            # plt.figure()
            x = np.arange(r)
            X, Y = np.meshgrid(x, y)
            # cs = plt.contourf(X, Y, Z[:, splits[i]:splits[i]+r], 4)
            # for cc in cs.collections:
            #     p = cc.get_paths()
            #     all_lines = []
            #     for pp in p:
            #         v = pp.vertices
            #         all_lines.extend(v[:, 1])
            #     lines.append(np.max(all_lines))
            #     lines.append(np.min(all_lines))
        lines = np.sort(np.unique(lines))
        c = 0
        while c < 10:
            b = abs(lines[:-1]-lines[1:])
            c = np.percentile(b, 1)
            lines = lines[np.where(b >= c)[0].tolist() + [-1]]
        x = np.arange(len(field_views))
        X, Y = np.meshgrid(x, y)
        # plt.contourf(X, Y, Z, 100)
        # for l in lines:
        #     plt.axhline(y=l, color='r', linestyle='-')

        # ====================
        # = save video flows =
        # ====================
        lines = lines[1:]
        lines += 360 * (lines < 0)
        lines -= 360 * (lines > 359)
        print(lines)
        for i in range(len(lines)):
            name = "stream" + str(i)
            if i == len(lines)-1:
                alpha = round(lines[i])
                beta = round(lines[0])-1
            else:
                alpha = round(lines[i])
                beta = round(lines[i+1])-1
            quality = 1
            streams.append(vf.VideoFlow(name, alpha, beta, quality))
    # plt.close('all')
    for s in streams:
        s.get_info()
    video_stream_definition_csv(streams)
    return streams, df, names, field_views, threshold


def get_trajectories(threshold=150):
    df = pd.read_csv("learnData.csv", header=None, delimiter=',')
    df.columns = ['name', 'time', 'angle']

    names = df['name'].unique().tolist()
    df.set_index(keys=['name'], drop=True, inplace=True)
    # df['angle'] = df['angle'].apply(lambda x: x - 360 * (x > threshold))
    return df, names


def sep_trajectory_by_name(df, names):
    all_dfs = []
    for name in names:
        exmpl_df = df.loc[df.name == name]
        exmpl_df.set_index(keys=['time'], drop=True, inplace=True)
        exmpl_df = exmpl_df.drop('name', 1)
        exmpl_df['angle'] = exmpl_df['angle'].astype(float)
        exmpl_df.index = pd.to_datetime(exmpl_df.index, unit='s')
        all_dfs.append(exmpl_df)
    return np.array(all_dfs)


def get_field_view(angles, is_standard=True):
    new_angles = []
    for angle in angles:
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        if is_standard:
            field_view = field_view * np.logical_and(0 <= field_view, field_view < 360).astype(int) +  \
                        (field_view + 360) * (field_view < 0).astype(int) + \
                        (field_view - 360) * (field_view > 359).astype(int)
        new_angles.append(field_view)
    return new_angles


def concat_times(df, threshold):
    field_views = []
    for i in np.arange(61):
        example_df = df.loc[df.time == i]
        field_view = example_df.viewfield.as_matrix().flatten() + 359 - threshold
        field_view = np.hstack(field_view)
        unique, counts = np.unique(field_view, return_counts=True)
        occurrences = dict(zip(unique, counts))
        axis_v = np.zeros(360)
        for v in np.arange(360):
            if v in occurrences:
                axis_v[v] = occurrences[v]
        field_views.append(axis_v)
    return pd.DataFrame(data=field_views, index=np.arange(61))


def video_stream_definition_csv(streams):
    s_names = []
    s_alphas = []
    s_betas = []
    for s in streams:
        s_names.append(s.name)
        s_alphas.append(s.alpha)
        s_betas.append(s.beta)
    streams_df = pd.DataFrame(index=s_names)
    streams_df["alpha"] = s_alphas
    streams_df["beta"] = s_betas
    streams_df.to_csv("video_stream_definition.csv")


if __name__ == '__main__':
    get_video_flows(uniform=False)
