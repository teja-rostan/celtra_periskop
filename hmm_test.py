from seqlearn.perceptron import StructuredPerceptron
import numpy as np
from video_flows_set import get_video_flows
from matplotlib import pyplot as plt
from scipy.stats import vonmises


def get_score(pred, test):
    score = 0
    for i, angle in enumerate(test.angle):
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        field_view = field_view * np.logical_and(0 <= field_view, field_view < 360).astype(int) + \
                     (field_view + 360) * (field_view < 0).astype(int) + \
                     (field_view - 360) * (field_view > 359).astype(int)
        for view in field_view:
            score += pred[i, int(view / 6)]
    return score


def get_video_quality(kappa, loc):
    # ==============
    # = parameters =
    # ==============
    B = 10000
    high = 100
    low = 26
    x = np.linspace(-np.pi, np.pi, 60)
    y = vonmises.pdf(x, kappa, loc)
    high = np.maximum(np.minimum(np.round(np.max(y)*80), high), 28)
    if high > 28:
        print(high)
    # ==========================================================
    # = quality distribution processing to meet our boundaries =
    # ==========================================================
    # y = np.minimum(y, np.percentile(y, 98))
    # y = np.minimum(y, np.max(y) * 0.95)
    mins = np.min(y)
    maxs = np.max(y)
    rng = maxs - mins
    y = np.ceil(high - (((high - low) * (maxs - y)) / rng)).astype(np.int)
    unique, counts = np.unique(y, return_counts=True)
    counts = dict(zip(unique, counts))
    to_cut = (sum(y) * 6 - B) / 6
    # print(to_cut, sum(y)*6, y)
    idx = low+1
    while to_cut > 0:
        if idx in counts:
            to_cut -= counts[idx] * (idx - low)
        idx += 1
    y[y < idx] = low
    if sum(y) * 6 > B:
        print("WAAAAAAAAAAAAAAAAAAAA!!!!!!!!!:", y, sum(y * 6), idx)
        plt.plot(np.degrees(x), y)
        plt.show()

    if B - sum(y)*6 > 280:
        print("LALALALALA:", B - sum(y)*6)
    return y


def one_hot(x):
    x_new = np.zeros((x.shape[0], 60))
    for i, idx in enumerate(x):
        x_new[i, idx] = 1
    return x_new


def half_sec_round(df):
    df.time = df.time * 2
    df.time = df.time.round()
    df.time = df.time * 0.5
    df = df.groupby([df.index, 'time']).mean()
    df = df.reset_index()
    df.set_index(keys=['name'], drop=True, inplace=True)
    return df

streams, df, names, field_views, threshold = get_video_flows(uniform=True)
df['angle'] = df['angle'].apply(lambda x: x + 360 * (x < 0))
m = 50
l = 10

df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]


def hmm_learner(df_before, df_after):
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time)
        y_train_df = half_sec_round(df_after.loc[df_after.index != name])
        y_train_df = y_train_df.loc[y_train_df.time.isin(y_test_df.time)]
        X_test_df = half_sec_round(df_before.loc[df_before.index == name])
        X_train_df = half_sec_round(df_before.loc[df_before.index != name])
        X_train_df = X_train_df.loc[X_train_df.time.isin(X_test_df.time)]
        lengths_train = y_train_df.groupby(level=0).size().as_matrix()
        lengths_test = y_test_df.groupby(level=0).size().as_matrix()

        X_train = one_hot((np.round(X_train_df.angle)/6).astype(np.int))
        X_test = one_hot((np.round(X_test_df.angle)/6).astype(np.int))
        y_test = (np.round(y_test_df.angle)/6).astype(np.int).as_matrix()
        y_train = (np.round(y_train_df.angle)/6).astype(np.int).as_matrix()
        # print(y_test.shape, y_train.shape, X_test.shape, X_train.shape, sum(lengths_train), sum(lengths_test))

        clf = StructuredPerceptron(verbose=False, max_iter=20)
        clf.fit(X_train, y_train, lengths_train)
        y_pred = clf.predict(X_test, lengths_test)
        # print("Accuracy: %.3f" % (100 * accuracy_score(y_test, y_pred)))
        # print(y_test)
        # print(y_pred)
        y_test_df["pred"] = y_pred
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t*2)/2
            the_row =y_test_df.loc[y_test_df.time == approx_t]
            # print(the_row)
            atan2_a = the_row['pred'] + 2.5
            kappa = 0.4
            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores
