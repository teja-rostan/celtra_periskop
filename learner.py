from video_flows_set import get_uniform_streams
import numpy as np
# from matplotlib import pyplot as plt
from scipy.stats import vonmises
import pandas as pd
import scipy.ndimage
from seqlearn import hmm
#import NNLearner
# from RNN import RNN_with_gating
# from keras.models import Sequential
# from keras.layers import Dense, Dropout
# from keras.layers import Embedding
# from keras.layers import LSTM

# namesto pol sekunde probaj četrt sekunde

# tokove zamakni za 3 stopinje! za 150 stopinj!

middle = 0
shift = 150
half = 4
l = 4

# 4:4
# 3:3
# 2:2
# 1:1


# =========================
# = load data and streams =
# =========================
streams, df, names = get_uniform_streams(shift)


def main():
    pd.options.mode.chained_assignment = None
    global streams, df, names, field_views, threshold, middle, l
    avg_score = []
    for m in range(4, 56, 4):
        middle = m
        names = np.array(names)
        np.random.shuffle(names)
        avg_four_score = []
        for l_now in [4, 3, 2, 1]:
            l = l_now

            df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
            df_after_dz = df.loc[np.logical_and(m +l-1 <= df.time, df.time < m + l)]
            df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]

            # df_before.angle = scipy.ndimage.gaussian_filter(df_before.angle, 3)
            scores_dz = dummy_zero(df_after_dz)
            scores_hmm = hmm_learner(df_before, df_after)
            perfect_scores = np.mean(df_after.groupby(df_after.index).size().as_matrix() * 60 * 100)

            avg_four_score.append(np.maximum(np.mean(scores_hmm), np.mean(scores_dz))/perfect_scores)

        avg_score.append(np.mean(avg_four_score))
    print(np.mean(avg_score))


def dummy_zero(df_after):
    global shift
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        pred_quality = np.zeros((df_after.time.shape[0], len(streams)))
        test = df_after.loc[df_after.index == name]
        for j, t in enumerate(test.time):
            # atan2_a = np.radians(0-shift+360)
            # atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            # y = get_video_quality(4, atan2_a)
            angle = 0-shift+360
            y = get_video_quality(4, angle - 360 * (angle > 359))
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, test)
    return scores


def one_hot(x):
    x_new = np.zeros((x.shape[0], 60))
    for i, idx in enumerate(x):
        x_new[i, idx] = 1
    return x_new


def add_missing(df, m_type):
    global middle, half, l
    all = l*half+1
    df_new = df.reset_index()
    sizes = df_new.groupby([df_new.name]).size()
    # print(sizes)
    outliers = sizes.loc[sizes != all]
    for index, outlier in outliers.iteritems():
        first = list(df_new.loc[df_new.name == index].index)[0]
        times = df_new.loc[df_new.name == index].time.as_matrix()
        angles = df_new.loc[df_new.name == index].angle.as_matrix()
        # print(len(angles))
        if m_type == "before":
            before = np.arange(middle-l, middle+0.1, step=1/half)
            for i, b in enumerate(before):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print(m_type, times, before)
        elif m_type == "after":
            after = np.arange(middle, middle+l+0.1, step=1/half)
            for i, b in enumerate(after):
                if b not in times:
                    if i == 0:
                        new_angle = angles[i]
                    elif i >= len(angles):
                        new_angle = angles[-1]
                    else:
                        new_angle = np.mean([angles[i - 1], angles[i]])
                    line = pd.DataFrame({"name": index, "time": b, "angle": new_angle}, index=[first+i])
                    old_df_len = len(df_new)
                    df_new = pd.concat([df_new.iloc[:first+i], line, df_new.iloc[first+i:]]).reset_index(drop=True)
                    if old_df_len != len(df_new)-1:
                        print(m_type, times, after)
    sizes = df_new.groupby([df_new.name]).size()
    outliers = sizes.loc[sizes != all]
    if len(outliers) != 0:
        print(outliers, m_type)
    return df_new.set_index(keys=['name'], drop=True)


def half_sec_round(df, m_type):
    global half
    df.time = df.time * half
    df.time = df.time.round()
    df.time = df.time / half
    df = df.groupby([df.index, 'time']).mean()
    df = df.reset_index()
    df.set_index(keys=['name'], drop=True, inplace=True)
    return add_missing(df, m_type)


def degree2stream(degrees):
    global shift
    degrees += shift
    degrees += 360 * (degrees < 0)
    degrees -= 360 * (degrees > 359)
    return np.floor(degrees / 6)


def hmm_learner(df_before, df_after):
    global half
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        y_train_df = half_sec_round(df_after.loc[df_after.index != name], "after")
        y_train_df = y_train_df[y_train_df.time.isin(y_test_df.time)]
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")
        X_train_df = half_sec_round(df_before.loc[df_before.index != name], "before")
        X_train_df = X_train_df[X_train_df.time.isin(X_test_df.time)]
        lengths_train = y_train_df.groupby(level=0).size().as_matrix()
        lengths_test = y_test_df.groupby(level=0).size().as_matrix()

        # X_test_df.angle = scipy.ndimage.gaussian_filter(X_test_df.angle, 3)
        # X_train_df.angle = scipy.ndimage.gaussian_filter(X_train_df.angle, 3)
        # y_train_df.angle = scipy.ndimage.gaussian_filter(y_train_df.angle, 3)

        X_train = one_hot(degree2stream(X_train_df.angle).astype(np.int))
        X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))
        y_train = degree2stream(y_train_df.angle).astype(np.int).as_matrix()

        clf = hmm.MultinomialHMM()
        # print("FIT")
        clf.fit(X_train, y_train, lengths_train)
        # print("PREDICT")
        y_pred = clf.predict(X_test, lengths_test)
        # print("SCORE")
        y_test_df["pred"] = y_pred[-1]
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            # print(j, t)
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(the_row['pred'].as_matrix()[0]*6 + 2.5)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores


def get_score(pred, test):
    score = 0
    for i, angle in enumerate(test.angle):
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        field_view += 360 * (field_view < 0)
        field_view -= 360 * (field_view > 359)
        for view in field_view:
            view = degree2stream(view)
            # print(view)
            score += pred[i, int(view)]
    return score


def hmm2_learner(df_before, df_after):
    global half
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        y_train_df = half_sec_round(df_after.loc[df_after.index != name], "after")
        y_train_df = y_train_df[y_train_df.time.isin(y_test_df.time)]
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")
        X_train_df = half_sec_round(df_before.loc[df_before.index != name], "before")
        X_train_df = X_train_df[X_train_df.time.isin(X_test_df.time)]
        lengths_train = y_train_df.groupby(level=0).size().as_matrix()
        lengths_test = y_test_df.groupby(level=0).size().as_matrix()

        X_train = one_hot(degree2stream(X_train_df.angle).astype(np.int))
        X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))
        y_train = degree2stream(y_train_df.angle).astype(np.int).as_matrix()

        clf = hmm.MultinomialHMM()

        clf.fit(X_train, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(X_test, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)
        y_fitt = clf.predict(X_train, lengths_train)
        y_pred = clf.predict(y_pred, lengths_test)
        y_fitt = one_hot(y_fitt.astype(np.int))
        y_pred = one_hot(y_pred.astype(np.int))

        clf.fit(y_fitt, y_train, lengths_train)

        y_pred = clf.predict(y_pred, lengths_test)

        y_test_df["pred"] = y_pred
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(the_row['pred'].as_matrix()[0]*6 + 2.5)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores


def get_score(pred, test):
    score = 0
    for i, angle in enumerate(test.angle):
        field_view = np.arange(start=angle - 30, stop=angle + 30, step=1)
        field_view += 360 * (field_view < 0)
        field_view -= 360 * (field_view > 359)
        for view in field_view:
            view = degree2stream(view)
            # print(view)
            score += pred[i, int(view)]
    return score


def get_video_quality(kappa, loc):
    # ==============
    # = parameters =
    # ==============
    B = 10000
    high = 100
    low = 1
    x = np.linspace(-np.pi, np.pi, 60)
    y = vonmises.pdf(x, kappa, loc)
    y = np.minimum(y, np.percentile(y, 85))
    # high = np.maximum(np.minimum(np.round(np.max(y)*160), high), 32)

    # ==========================================================
    # = quality distribution processing to meet our boundaries =
    # ==========================================================
    # y = np.minimum(y, np.percentile(y, 98))
    # y = np.minimum(y, np.max(y) * 0.95)
    mins = np.min(y)
    maxs = np.max(y)
    rng = maxs - mins
    y = np.ceil(high - (((high - low) * (maxs - y)) / rng)).astype(np.int)
    # unique, counts = np.unique(y, return_counts=True)
    # counts = dict(zip(unique, counts))
    # to_cut = (sum(y) * 6 - B) / 6
    to_cut = (sum(y*6) - B)
    y_sort = np.argsort(y)
    idx=0
    while to_cut > 0:
        to_cut -= y[y_sort[idx]]*6 - low*6
        y[y_sort[idx]] = low
        idx += 1
    # idx = low+1
    # while to_cut > 0:
    #     if idx in counts:
    #         to_cut -= counts[idx] * (idx - low)
    #     idx += 1
    # y[y < idx] = low
    if sum(y) * 6 > B:
        print("ERROR!, sum of quality above upper bound!:", y, sum(y * 6), idx)
        # plt.plot(np.degrees(x), y)
        # plt.show()
        return
    if B - sum(y)*6 > 280:
        print("WARNING!, sum of quality is low!:", B - sum(y*6))
    return np.roll(y, -(int(len(y)/2)))


if __name__ == '__main__':
    main()


