import matplotlib.pyplot as plt
import numpy as np

# ==============
# = parameters =
# ==============
fileName = "learnData.csv"
printOutCSV = False
sessionToDraw = []  # ["s1490363010x55b4b9e68abc1bx66279813"] #[]  # empty list to draw all
savePlot = False  # it will display the graph if False, save to file if True

# =============
# = load data =
# =============
file = open(fileName, "r")
sessions = {}
TIME = "time"
ANGLE = "angle"
for line in file:
    values = line.split(",")
    sessionId = values[0]
    angleDeg = int(values[2])
    angleRad = angleDeg/360. * 2 * np.pi
    time = float(values[1])
    if printOutCSV:
        print("%s,\t%f,\t%d" % (sessionId, time, angleDeg))
    if sessionId in sessions:
        sessions[sessionId][TIME].append(time)
        sessions[sessionId][ANGLE].append(angleRad)
    else:
        sessions[sessionId] = {TIME: [time], ANGLE: [angleRad]}
print("Done reading %d sessions." % (len(sessions)))

# =============
# = plot data =
# =============
# graphs = 0
# for session in sessions:
#     if (session in sessionToDraw) or (not sessionToDraw):
#         graphs += 1
#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='polar')
#         ax.plot(sessions[session][ANGLE], sessions[session][TIME], color="black", linewidth=1, alpha=0.3)
#         ax.set_theta_zero_location("N")
#         ax.set_theta_direction(-1)
#         ax.set_rmin(-30.0)
#         if (savePlot):
#             plt.savefig(session + '.png')
#         else:
#             plt.show()
# print("Done drawing %d graphs." % (graphs))

graphs = 0
fig = plt.figure()
ax = fig.add_subplot(111, projection='polar')
ax.set_theta_zero_location("N")
ax.set_theta_direction(-1)
ax.set_rmin(-30.0)
idx = 0
for session in sessions:
    if (session in sessionToDraw) or (not sessionToDraw):
        graphs += 1

        ax.plot(sessions[session][ANGLE], sessions[session][TIME], linewidth=1, alpha=0.3)
        idx += 1
        if idx > 5:
            idx = 0
            plt.show()
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='polar')
            ax.set_theta_zero_location("N")
            ax.set_theta_direction(-1)
            ax.set_rmin(-30.0)
            idx = 0

if savePlot:
    plt.savefig(session + '.png')
else:
    plt.show()
print("Done drawing %d graphs." % graphs)
