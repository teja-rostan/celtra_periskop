"""
Prvi del vaše naloge je
določiti množico video tokov s_i = (α_i,β_i,q_i),
pri čemer vsak tok pokriva kot med α in β
(vkljucno) s kvaliteto q ∈ [1,100].
Video tokovi se lahko prekrivajo (izbre se tistega, ki ima najvišjo kvaliteto
Število video tokov, izbranih v prvem delu naloge, ne sme biti večje kot 100!
Imamo samo en videoposnetek in več uporabnikov.
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from pandas.tools.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error as mse
import time

from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization!

from sklearn.feature_extraction import image
from sklearn.cluster import spectral_clustering


threshold = 150


def get_viewfield(angles, is_standard=True):
    new_angles = []
    for angle in angles:
        viewfield = np.arange(start=angle - 30, stop=angle + 30, step=1)
        if is_standard:
            viewfield = viewfield * np.logical_and(0 <= viewfield, viewfield < 360).astype(int) +  \
                        (viewfield + 360) * (viewfield < 0).astype(int) + \
                        (viewfield - 360) * (viewfield > 359).astype(int)
        new_angles.append(viewfield)
    # return np.array(new_angles).flatten()
    return new_angles


def concat_times(df):
    viewfields = []
    for i in np.arange(61):
        exmpl_df = df.loc[df.time == i]
        viewfield = exmpl_df.viewfield.as_matrix().flatten() + 359 - threshold
        viewfield = np.hstack(viewfield)
        unique, counts = np.unique(viewfield, return_counts=True)
        occurances = dict(zip(unique, counts))
        axis_v = np.zeros(360)
        for v in np.arange(360):
            if v in occurances:
                axis_v[v] = occurances[v]
        viewfields.append(axis_v)
    return pd.DataFrame(data=viewfields, index=np.arange(61))

df = pd.read_csv("learnData.csv", header=None, delimiter=',')
df.columns = ['name', 'time', 'angle']

df.set_index(keys=['name'], drop=False, inplace=True)
names = df['name'].unique().tolist()
df['angle'] = df['angle'].apply(lambda x: x - 360 * (x > threshold))

all_dfs = []
for name in names:
    exmpl_df = df.loc[df.name == name]
    exmpl_df.set_index(keys=['time'], drop=True, inplace=True)
    exmpl_df = exmpl_df.drop('name', 1)
    exmpl_df['angle'] = exmpl_df['angle'].astype(float)
    exmpl_df.index = pd.to_datetime(exmpl_df.index, unit='s')
    # print(exmpl_df)
    all_dfs.append(exmpl_df)

# for one_df in all_dfs:
#     angles = one_df.angle.as_matrix()
#     plt.plot(angles)
# plt.show()

# for one_df in all_dfs:
#    angles = one_df.angle.as_matrix()
#    autocorrelation_plot(angles) # lag = 30
#    plt.show()

angles = df.angle
print(angles.shape)
new_angles = get_viewfield(angles)
new_angles = np.array(new_angles).flatten()
print(new_angles.shape, np.any(new_angles < 0), np.any(new_angles > 359))
plt.hist(new_angles, bins=360, normed=True)  # plt.hist passes it's arguments to np.histogram
plt.title("Histogram with '360' degrees, the fieldview")
plt.axis([0, 359, 0, 0.01])
# plt.show()

df_plot = df.copy()
# df_plot.sort("time", inplace=True)
df_plot.time = df_plot.time.round()
new_angles = get_viewfield(df_plot.angle, is_standard=False)
df_plot["viewfield"] = new_angles
# naredi za vsako sekundo array od -159 do 200 in dodaj count za vsak kot
viewfields = concat_times(df_plot)

lines = []
print(viewfields)
Z = viewfields.as_matrix().T
Z = (Z - Z.min()) / (Z.max() - Z.min())
y = np.arange(-(359 - threshold), threshold + 1)
splits = np.round(np.linspace(0, len(viewfields), 10))
ranges = splits[1:] - splits[:-1]
print(splits)
print(ranges)
for i, r in enumerate(ranges):
    fig = plt.figure()
    x = np.arange(r)
    X, Y = np.meshgrid(x, y)
    print(X.shape)
    print(Y.shape)
    print(Z.shape)
    cs = plt.contourf(X, Y, Z[:, splits[i]:splits[i]+r], 4)
    # plt.show()
    for cc in cs.collections:
        p = cc.get_paths()
        minv = 300
        maxv = -300
        for pp in p:
            v = pp.vertices
            y2 = v[:, 1]
            lmax = np.max(y2)
            lmin = np.min(y2)
            if lmax > maxv:
                maxv = lmax
            if lmin < minv:
                minv = lmin
        lines.append(maxv)
        lines.append(minv)

lines = np.sort(np.unique(lines))
c = 0
while c < 10:
    b = abs(lines[:-1]-lines[1:])
    c = np.percentile(b, 1)
    lines = lines[np.where(b >= c)[0].tolist() + [-1]]

x = np.arange(len(viewfields))
X, Y = np.meshgrid(x, y)
cs = plt.contourf(X, Y, Z, 100)
print(len(lines))
for l in lines:
    plt.axhline(y=l, color='r', linestyle='-')
print(lines)
plt.show()

# k_means = KMeans(init='k-means++', n_clusters=100, n_init=1000)
# k_means.fit(Z)
# k_means_labels = k_means.labels_
# k_means_cluster_centers = k_means.cluster_centers_
# k_means_labels_unique = np.unique(k_means_labels)
#
# colors = np.arange(100)#['#4EACC5', '#FF9C34', '#4E9A06']
# plt.figure()
# plt.hold(True)
# for k, col in zip(range(100), colors):
#     my_members = k_means_labels == k
#     cluster_center = k_means_cluster_centers[k]
#     plt.plot(X[my_members, 0], X[my_members, 1], 'w',
#              marker='.')
#     plt.plot(cluster_center[0], cluster_center[1], 'o',
#             markeredgecolor='k', markersize=6)
# plt.title('KMeans')
# plt.grid(True)
# plt.show()
# idx = 0
# for dd in all_dfs:
#     plt.plot(dd.angle)
    # idx+=1
    # if idx > 5:
    #     idx=0
    #     plt.show()
# plt.show()

# df.time = df.time.round(decimals=1)
# print(df)


def arima_prediction(all_dfs):
    X = all_dfs[0].angle.values
    size = int(len(X) * 0.66)
    train, test = X[0:size], X[size:len(X)]
    history = train.tolist()

    model = ARIMA(history, order=(2, 1, 0))  # mora biti 30, povedala avtokorelacija in drugi mora biti 0, ker je
    # stacionarna (povprečje in varianca ostaja enaka (ni nekega trenda k dvihu ali nižanju)
    model_fit = model.fit(disp=True)
    print(size, len(X))
    predictions = model_fit.predict(size, len(X) - 1, dynamic=True)
    print(predictions)
    predictions = pd.read_csv("results.csv", index_col=0, header=None).as_matrix().flatten()
    print(predictions)
    error = mse(test, predictions)
    print('Test MSE: %.3f' % error)
    results = pd.Series(predictions)
    results.to_csv("results" + time.strftime("%d_%m_%Y") + ".csv")
    plt.plot(test)
    plt.plot(predictions, color='red')
    plt.show()

#prvi del - razdeli posnete na kose

#drugi del - naredi mean learner


