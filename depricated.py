
def half_sec_round(df):
    df.time = df.time * 2
    # df.time = df.time.round(decimals=1)
    df.time = df.time * 0.5
    df = df.groupby([df.index, 'time']).mean()
    df = df.reset_index()
    df.set_index(keys=['name'], drop=True, inplace=True)
    return df

def round_and_complete(df_before, df_after, name):
    y_test_df_original = df_after.loc[df_after.index == name]

    y_test_df = half_sec_round(y_test_df_original)
    y_train_df = half_sec_round(df_after.loc[df_after.index != name])
    X_test_df = half_sec_round(df_before.loc[df_before.index == name])
    X_train_df = half_sec_round(df_before.loc[df_before.index != name])

    X_train_df = add_missing_angles(X_train_df, X_test_df)
    y_train_df = add_missing_angles(y_train_df, y_test_df)

    y_train_df = y_train_df[y_train_df.time.isin(y_test_df.time)]
    X_train_df = X_train_df[X_train_df.time.isin(X_test_df.time)]

    # lengths_train = y_train_df.groupby(level=0).size().as_matrix()
    lengths_train = X_train_df.groupby(level=0).size().as_matrix()
    lengths_test = y_train_df.groupby(level=0).size().as_matrix()
    return X_train_df, X_test_df, y_train_df, y_test_df, lengths_train, lengths_test, y_test_df_original


def add_missing_angles(train, test):
    new_names = []
    new_times = []
    new_angles = []
    groups = train.groupby(level=0)
    for name, group in groups:
        missing = test[~test.time.isin(group.time)]
        for index, m in missing.iterrows():
            t = m.time
            idx = np.where(group.time.as_matrix() > t)[0]
            if idx.size > 0:
                idx = idx[0]
                new_names.append(name)
                new_times.append(t)
                new_angles.append(np.mean(group.angle.as_matrix()[np.maximum(idx-1,0):np.minimum(idx+1, len(group.angle))]))
            else:
                new_names.append(name)
                new_times.append(t)
                if t > group.time.as_matrix()[-1]:
                    new_angles.append(group.angle.as_matrix()[-1])
                else:
                    new_angles.append(group.angle.as_matrix()[0])
    new_rows = pd.DataFrame({'time': new_times, 'angle': new_angles}, index=new_names)
    train = train.append(new_rows)
    # train.sort_index(inplace=True)
    train["name"] = train.index
    train.sort_values(['name', 'time'], inplace=True)
    train.drop('name', axis=1, inplace=True)
    return train


def mean_learner(df_after):
    scores = np.zeros((len(names), 1))
    mean_kappa = []
    for i, name in enumerate(names):

        # ===============================
        # = split to train and test set =
        # ===============================
        test = df_after.loc[df_after.index == name]
        train = df_after.loc[df_after.index != name]
        train.angle = scipy.ndimage.gaussian_filter(train.angle, 3)

        pred_quality = np.zeros((test.time.shape[0], len(streams)))

        for j, t in enumerate(test.time):

            # =========================================
            # = get mean and kappa=1/sd^2 and predict =
            # =========================================
            t_train = train.loc[train.time == t]
            t_angle = t_train.angle.as_matrix()

            sin_a = np.sin(np.radians(t_angle)).sum()
            cos_a = np.cos(np.radians(t_angle)).sum()
            atan2_a = np.arctan2(sin_a, cos_a)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            R = np.sqrt(np.square(sin_a) + np.square(cos_a)) / len(t_angle)
            kappa = 1 / (-2 * np.log(R))

            mean_kappa.append(kappa)
            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        # df_quality = pd.DataFrame(pred_quality, index=test.time, columns=get_stream_names())
        scores[i] = get_score(pred_quality, test)
    return scores


def weighted_mean_learner(df_before, df_after):
    mean_kappa = []
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):

        # ===============================
        # = split to train and test set =
        # ===============================
        test_after = df_after.loc[df_after.index == name]
        test_before = df_before.loc[df_before.index == name]
        train_after = df_after.loc[df_after.index != name]
        train_before = df_before.loc[df_before.index != name]
        train_after.angle = scipy.ndimage.gaussian_filter(train_after.angle, 3)
        train_before.angle = scipy.ndimage.gaussian_filter(train_before.angle, 3)

        pred_quality = np.zeros((test_after.time.shape[0], len(streams)))
        weights = pd.DataFrame(index=test_before.time, columns=np.delete(names, i))

        for j, (t, a) in enumerate(zip(test_before.time, test_before.angle)):

            # ====================================================
            # = get weights for every instance in train data set =
            # ====================================================
            t_train = train_before.loc[train_before.time == t]
            a = t_train.angle - a
            a = abs((a + 180) % 360 - 180)

            t_train = t_train.assign(distance=a)
            t_train = t_train.groupby(t_train.index).mean()

            for index, row in t_train.iterrows():
                weights.set_value(t, index, row["distance"])

        weights = weights.mean(axis=0)
        weights = np.square(1 - (weights - weights.min()) / (weights.max() - weights.min()))

        for j, t in enumerate(test_after.time):

            # =========================================
            # = get mean and kappa=1/sd^2 and predict =
            # =========================================
            t_train = train_after.loc[train_after.time == t]
            t_angle = t_train.angle.as_matrix()
            t_train = pd.merge(left=t_train, left_index=True, right=weights.to_frame(), right_index=True, how='inner')
            t_train = t_train.rename(columns={0: 'distance'})
            t_distance = t_train.distance.as_matrix()

            sin_a = np.dot(np.sin(np.radians(t_angle)), t_distance)
            cos_a = np.dot(np.cos(np.radians(t_angle)), t_distance)
            atan2_a = np.arctan2(sin_a, cos_a)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            R = np.sqrt(np.square(sin_a) + np.square(cos_a)) / np.sum(t_distance)
            kappa = 1 / (-2 * np.log(R))
            mean_kappa.append(kappa)

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y

        scores[i] = get_score(pred_quality, test_after)
    return scores


def rnn_learner(df_before, df_after):
    global half
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        print(str(int(i/len(names)*100)) + "%")
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        y_train_df = half_sec_round(df_after.loc[df_after.index != name], "after")
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")
        X_train_df = half_sec_round(df_before.loc[df_before.index != name], "before")

        lengths = X_test_df.groupby(level=0).size().as_matrix()[0]

        X_train = degree2stream(X_train_df.angle).astype(np.int)
        X_test = degree2stream(X_test_df.angle).astype(np.int)
        y_train = degree2stream(y_train_df.angle).astype(np.int)
        y_test = degree2stream(y_test_df.angle).astype(np.int)

        X_train = pd.DataFrame({"angle": X_train, "attr": np.tile(np.arange(lengths), len(names)-1)}, index=X_train.index)
        X_train = X_train.pivot(index=X_train.index, columns='attr')
        X_test = pd.DataFrame({"angle": X_test, "attr": np.arange(lengths)}, index=X_test.index)
        X_test = X_test.pivot(index=X_test.index, columns='attr')
        y_train = pd.DataFrame({"angle": y_train, "attr": np.tile(np.arange(lengths), len(names)-1)}, index=y_train.index)
        y_train = y_train.pivot(index=y_train.index, columns='attr')

        # reshape input to be [samples, time steps, features]
        X_train = np.reshape(X_train.as_matrix(), (X_train.shape[0], 1, X_train.shape[1]))
        X_test = np.reshape(X_test.as_matrix(), (X_test.shape[0], 1, X_test.shape[1]))

        # model = Sequential()
        # model.add(LSTM(20, input_shape=(1, lengths), return_sequences=True))
        # model.add(LSTM(20, return_sequences=True))
        # model.add(LSTM(20))
        # model.add(Dense(lengths))
        # model.compile(optimizer='rmsprop', loss='mean_squared_error')

        # model.fit(X_train, y_train.as_matrix(), epochs=1, batch_size=9, verbose=0)

        # y_pred = model.predict(X_test)

        # y_test_df["pred"] = y_pred.flatten()
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(the_row['pred'].as_matrix()[0] * 6 + 2.5)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores



def dummy(df_after):
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        test = df_after.loc[df_after.index == name]
        pred_quality = np.ones((test.time.shape[0], len(streams)))*27
        scores[i] = get_score(pred_quality, test)
    return scores



def nn_learner(df_before, df_after):
    global half
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        y_train_df = half_sec_round(df_after.loc[df_after.index != name], "after")
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")
        X_train_df = half_sec_round(df_before.loc[df_before.index != name], "before")

        lengths = X_test_df.groupby(level=0).size().as_matrix()[0]

        X_train = degree2stream(X_train_df.angle).astype(np.int)
        X_test = degree2stream(X_test_df.angle).astype(np.int)
        y_train = degree2stream(y_train_df.angle).astype(np.int)
        y_test = degree2stream(y_test_df.angle).astype(np.int)
        #
        X_train = pd.DataFrame({"angle": X_train, "attr": np.tile(np.arange(lengths), len(names)-1)}, index=X_train.index)
        X_train = X_train.pivot(index=X_train.index, columns='attr')
        X_test = pd.DataFrame({"angle": X_test, "attr": np.arange(lengths)}, index=X_test.index)
        X_test = X_test.pivot(index=X_test.index, columns='attr')
        y_train = pd.DataFrame({"angle": y_train, "attr": np.tile(np.arange(lengths), len(names)-1)}, index=y_train.index)
        y_train = y_train.pivot(index=y_train.index, columns='attr')

        nn = NNLearner.NNLearner_reg(lengths, lengths, 1, 10)

        # print(X_train.as_matrix()[0])
        # print(y_train.as_matrix()[0])
        nn.fit(X_train.as_matrix(), y_train.as_matrix())

        y_pred = nn.predict(X_test.as_matrix())
        # print(y_pred.flatten())
        # print(y_test.as_matrix())
        y_test_df["pred"] = y_pred.flatten()
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(the_row['pred'].as_matrix()[0] * 6 + 2.5)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores


def fit_nn(df, names):
    global middle
    l = 4
    # lengths = l * half + 1
    # nn = NNLearner.NNLearner_reg(lengths, lengths, 1, 10)
    clf = hmm.MultinomialHMM()
    for m in range(4, 56, 1):
        middle = m
        df_after = df.loc[np.logical_and(m <= df.time, df.time < m + l)]
        df_before = df.loc[np.logical_and(m - l <= df.time, df.time < m)]
        after_slice_df = half_sec_round(df_after, "after")
        before_slice_df = half_sec_round(df_before, "before")
        lengths_train = before_slice_df.groupby(level=0).size().as_matrix()

        X_train = one_hot(degree2stream(before_slice_df.angle).astype(np.int))
        y_train = degree2stream(after_slice_df.angle).astype(np.int).as_matrix()
        # after_slice = degree2stream(after_slice_df.angle).astype(np.int)
        # before_slice = degree2stream(before_slice_df.angle).astype(np.int)

        # after_slice = pd.DataFrame({"angle": after_slice, "attr": np.tile(np.arange(lengths), len(names))}, index=after_slice.index)
        # after_slice = after_slice.pivot(index=after_slice.index, columns='attr')
        # print(before_slice.index)
        # before_slice = pd.DataFrame({"angle": before_slice, "attr": np.tile(np.arange(lengths), len(names))}, index=before_slice.index)
        # before_slice = before_slice.pivot(index=before_slice.index, columns='attr')

        # after_slice["time"] = after_slice_df.time.as_matrix()[0]
        # before_slice["time"] = before_slice_df.time.as_matrix()[0]

        # nn.fit(before_slice.as_matrix(), after_slice.as_matrix())
        # print(X_train.shape, y_train.shape, lengths_train)
        clf.fit(X_train, y_train, lengths_train)
    return clf


def predict_nn(df_before, df_after, nn):
    global half
    scores = np.zeros((len(names), 1))
    for i, name in enumerate(names):
        y_test_df_time = df_after.loc[df_after.index == name]
        y_test_df = half_sec_round(y_test_df_time, "after")
        X_test_df = half_sec_round(df_before.loc[df_before.index == name], "before")

        # lengths = X_test_df.groupby(level=0).size().as_matrix()[0]
        lengths = X_test_df.groupby(level=0).size().as_matrix()

        # X_test = degree2stream(X_test_df.angle).astype(np.int)
        # y_test = degree2stream(y_test_df.angle).astype(np.int)
        X_test = one_hot(degree2stream(X_test_df.angle).astype(np.int))


        # X_test = pd.DataFrame({"angle": X_test, "attr": np.arange(lengths)}, index=X_test.index)
        # X_test = X_test.pivot(index=X_test.index, columns='attr')

        # y_pred = nn.predict(X_test.as_matrix())
        y_pred = nn.predict(X_test, lengths)

        y_test_df["pred"] = y_pred.flatten()
        pred_quality = np.zeros((y_test_df_time.time.shape[0], len(streams)))
        for j, t in enumerate(y_test_df_time.time):
            approx_t = np.round(t * half) / half
            the_row = y_test_df.loc[y_test_df.time == approx_t]
            atan2_a = np.radians(the_row['pred'].as_matrix()[0] * 6 + 2.5)
            atan2_a -= 2 * np.pi * (atan2_a > np.pi)
            kappa = 4

            y = get_video_quality(kappa, atan2_a)
            pred_quality[j] = y
        scores[i] = get_score(pred_quality, y_test_df_time)
    return scores


